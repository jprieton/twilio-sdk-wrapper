<?php

/**
 * Plugin Name: 		Twilio SDK Wrapper
 * Plugin URI: 			https://gitlab.com/jprieton/twilio-sdk-wrapper
 * Description: 		A WordPress plugin
 * Version:  			1.2.0
 * Requires at least:	5.2
 * Tested up to:		5.7
 * Author:				Javier Prieto
 */

// If this file is called directly, abort.
defined('ABSPATH') || exit;

define('TWILIO_SDK_WRAPPER_FILENAME', __FILE__);

// Autoloader
require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

// Init the plugin
if (is_admin()) {
	new TwilioSDKWrapper\Init;
}
