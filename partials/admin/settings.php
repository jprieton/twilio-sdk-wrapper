<div class="wrap">
	<h1>Twilio SDK Wrapper</h1>

	<form method="post" action="options.php">
		<?php
		settings_fields(self::MENU_SLUG . '_api_settings');
		do_settings_sections(self::MENU_SLUG . '_api_settings');
		?>

		<h3>API Settings</h3>
		<table class="form-table">
			<?php
			$fields = [
				[
					'id'    => self::MENU_SLUG . '_sid',
					'label' => 'Twilio Account SID'
				],
				[
					'id'    => self::MENU_SLUG . '_token',
					'label' => 'Twilio Account Token'
				],
			];

			?>

			<?php
			foreach ($fields as $field) {
				list('id' => $id, 'label' => $label) = $field;
			?>
				<tr valign="top">
					<th scope="row"><?php echo $label ?></th>
					<td><input type="text" name="<?php echo $id ?>" value="<?php echo esc_attr(get_option($id)); ?>" /></td>
				</tr>
			<?php
			}
			?>

			<tr valign="top">
				<th scope="row">Enabled</th>
				<td>
					<label for="<?php echo self::MENU_SLUG . '_enabled' ?>">
						<input name="<?php echo self::MENU_SLUG . '_enabled' ?>" type="checkbox" id="<?php echo self::MENU_SLUG . '_enabled' ?>" value="1" <?php echo checked(get_option(self::MENU_SLUG . '_enabled')) ?>>
						Allows to the Twilio SDK send messages
					</label>
				</td>
			</tr>

		</table>

		<h3>Sender Settings</h3>
		<table class="form-table">
			<?php
			$fields = [
				[
					'id'    => self::MENU_SLUG . '_sender',
					'label' => 'Phone Sender'
				],
			];
			?>

			<?php
			foreach ($fields as $field) {
				list('id' => $id, 'label' => $label) = $field;
			?>
				<tr valign="top">
					<th scope="row"><?php echo $label ?></th>
					<td><input type="text" name="<?php echo $id ?>" value="<?php echo esc_attr(get_option($id)); ?>" /></td>
				</tr>
			<?php
			}
			?>
		</table>

		<?php submit_button(); ?>

	</form>
</div>
