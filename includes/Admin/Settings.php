<?php

namespace TwilioSDKWrapper\Admin;

class Settings
{
	/**
	 * Slug of the settings page.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	const MENU_SLUG = 'twilio_sdk_wrapper_settings';

	/**
	 * Class constructor.
	 */
	public function __construct()
	{
		// Add setting menu item
		add_action("admin_menu", [$this, "admin_menu"]);
		// Saves and update settings
		add_action("admin_init", [$this, 'admin_settings_init']);
	}

	/**
	 * Add a menu item to the settings page.
	 *
	 * @since 1.0.0
	 * @return void
	 */
	public function admin_menu()
	{
		add_options_page(
			'Twilio SDK',
			'Twilio SDK',
			'manage_options',
			self::MENU_SLUG,
			[$this, 'display_settings_page']
		);
	}

	public function display_settings_page()
	{
		// Get the page settings template
		include_once plugin_dir_path(TWILIO_SDK_WRAPPER_FILENAME) . 'partials/admin/settings.php';
	}

	/**
	 * Registers and Defines the necessary fields we need.
	 *  @since    1.0.0
	 */
	public function admin_settings_init()
	{
		register_setting(
			self::MENU_SLUG . '_api_settings',
			self::MENU_SLUG . '_enabled',
			[
				'type' => 'string',
				'description' => 'Enable/Disable the Twilio SDK',
			]
		);
		register_setting(
			self::MENU_SLUG . '_api_settings',
			self::MENU_SLUG . '_sid',
			[
				'type' => 'string',
				'description' => 'Twilio Account SID',
			]
		);

		register_setting(
			self::MENU_SLUG . '_api_settings',
			self::MENU_SLUG . '_token',
			[
				'type' => 'string',
				'description' => 'Twilio Account Token',
			]
		);
		register_setting(
			self::MENU_SLUG . '_api_settings',
			self::MENU_SLUG . '_sender',
			[
				'type' => 'string',
				'description' => 'Phone Sender',
			]
		);
	}

	/**
	 * Sanitizes all input fields.
	 *
	 */
	public function plugin_options_validate($input)
	{
		$newinput["api_sid"] = trim($input["api_sid"]);
		$newinput["api_auth_token"] = trim($input["api_auth_token"]);
		return $newinput;
	}
}
