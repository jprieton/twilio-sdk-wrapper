<?php

function send_message_whatsapp($phone, $body)
{
	// Check if the SDK is enabled
	$enabled = get_option('twilio_sdk_wrapper_settings_enabled');
	if (empty($enabled)) {
		// The SDK is not enabled, nothing to do
		return false;
	}

	# Check ih there is a phone number
	$sender_phone = get_option('twilio_sdk_wrapper_settings_sender');
	if (empty($sender_phone)) {
		return false;
	}

	try {
		$twilio = TwilioSDKWrapper\TwilioSDKFactory::get_rest_client();
		$message = $twilio->messages
			->create(
				"whatsapp:+57{$phone}", // to
				[
					"from" => "whatsapp:{$sender_phone}",
					"body" => $body
				]
			);
		return $message->sid;
	} catch (Exception $e) {
		//TODO Agregar mensaje de error al log
		//wp_send_json_error(['message' => $e->getMessage()]);

		// var_dump($e);
		return false;
	}

	return true;
}
