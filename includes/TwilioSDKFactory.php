<?php
namespace TwilioSDKWrapper;

use Twilio\Rest\Client;
use Exception;

class TwilioSDKFactory {

	/**
	 * Undocumented function
	 *
	 * @return Client
	 * @throws Exception
	 */
	public static function get_rest_client() {
		$sid = get_option('twilio_sdk_wrapper_settings_sid');
		$token = get_option('twilio_sdk_wrapper_settings_token');

		if (empty($sid)) {
			throw new Exception('Twilio SID is not set');
		}
		if (empty($token)) {
			throw new Exception('Twilio Token is not set');
		}
		return new Client($sid, $token);
	}
}
